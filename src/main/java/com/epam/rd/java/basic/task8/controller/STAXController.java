package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Flower flower;
	private VisualParameters visualParameters;
	private AveLenFlower aveLenFlower;
	private GrowingTips growingTips;
	private Temperature temperature;
	private Lighting lighting;
	private Watering watering;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parse (){
		Flowers flowers = new Flowers();

		List<Flower> flowerList = new ArrayList<>();
		String currentElement;
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader reader = null;

		try {
			reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

			// move forward through the stream with nextEvent()
			while (reader.hasNext()){
				XMLEvent event = reader.nextEvent();

				// skip any empty content
				if (event.isCharacters() && event.asCharacters().isWhiteSpace())
					continue;

				// check if we've reached an start tag
				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					currentElement = startElement.getName().getLocalPart();

					//  find our desired start tag
					switch (currentElement) {
						case ParserConstants.FLOWER:
							flower = new Flower();
							break;
						case ParserConstants.NAME:
							event = reader.nextEvent();
							flower.setName(event.asCharacters().getData());
							break;
						case ParserConstants.SOIL:
							event = reader.nextEvent();
							flower.setSoil(event.asCharacters().getData());
							break;
						case ParserConstants.ORIGIN:
							event = reader.nextEvent();
							flower.setOrigin(event.asCharacters().getData());
							break;
						case ParserConstants.VISUAL_PARAMETERS:
							visualParameters = new VisualParameters();
							flower.setVisualParameters(visualParameters);
							break;
						case ParserConstants.STEM_COLOUR:
							event = reader.nextEvent();
							visualParameters.setStemColour(event.asCharacters().getData());
							break;
						case ParserConstants.LEAF_COLOUR:
							event = reader.nextEvent();
							visualParameters.setLeafColour(event.asCharacters().getData());
							break;
						case ParserConstants.AVE_LEN_FLOWER:
							aveLenFlower = new AveLenFlower();
							aveLenFlower.setMeasure(startElement.getAttributeByName(new QName(ParserConstants.MEASURE)).getValue());
							event = reader.nextEvent();
							aveLenFlower.setValue(Integer.valueOf(event.asCharacters().getData()));
							flower.getVisualParameters().setAveLenFlower(aveLenFlower);
							break;
						case ParserConstants.GROWING_TIPS:
							growingTips = new GrowingTips();
							flower.setGrowingTips(growingTips);
							break;
						case ParserConstants.TEMPERATURE:
							temperature = new Temperature();
							temperature.setMeasure(startElement.getAttributeByName(new QName(ParserConstants.MEASURE)).getValue());
							event = reader.nextEvent();
							temperature.setValue(Integer.valueOf(event.asCharacters().getData()));
							growingTips.setTemperature(temperature);
							break;
						case ParserConstants.LIGHTING:
							lighting = new Lighting();
							lighting.setLightRequiring(startElement.getAttributeByName(new QName(ParserConstants.LIGHT_REQUIRING)).getValue());
							growingTips.setLighting(lighting);
							break;
						case ParserConstants.WATERING:
							watering = new Watering();
							watering.setMeasure(startElement.getAttributeByName(new QName(ParserConstants.MEASURE)).getValue());
							event = reader.nextEvent();
							watering.setValue(Integer.valueOf(event.asCharacters().getData()));
							growingTips.setWatering(watering);
							break;
						case ParserConstants.MULTIPLYING:
							event = reader.nextEvent();
							flower.setMultiplying(event.asCharacters().getData());
							break;
						default:
							break;
					}
				}

				// check if we've reached an end tag
				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					currentElement = endElement.getName().getLocalPart();
					if (ParserConstants.FLOWER.equals(currentElement)) {
						flowerList.add(flower);
					}
				}

			}

		} catch (XMLStreamException e) {
			e.printStackTrace();
		} finally {
			if (reader != null){
				try {
					reader.close();
				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
			}
		}

		flowers.setFlowers(flowerList);
		return flowers;
	}

}