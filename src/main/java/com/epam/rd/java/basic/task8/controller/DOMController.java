package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.*;
import org.w3c.dom.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parse() {
		Flowers flowers = new Flowers();
		List<Flower> flowerList = new ArrayList<>();

		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(new File(xmlFileName));
			document.getDocumentElement().normalize();

			// returns a list of nodes of specified name
			NodeList nodeList = document.getElementsByTagName(ParserConstants.FLOWER);

			// loop by nodes
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				Flower flower = new Flower();

				Element element = (Element) node;
				// get elements from xml
				flower.setName(element.getElementsByTagName(ParserConstants.NAME).item(0).getTextContent());
				flower.setSoil(element.getElementsByTagName(ParserConstants.SOIL).item(0).getTextContent());
				flower.setOrigin(element.getElementsByTagName(ParserConstants.ORIGIN).item(0).getTextContent());

				VisualParameters visualParameters = new VisualParameters();
				AveLenFlower aveLenFlower = new AveLenFlower();
				visualParameters.setStemColour(element.getElementsByTagName(ParserConstants.STEM_COLOUR).item(0).getTextContent());
				visualParameters.setLeafColour(element.getElementsByTagName(ParserConstants.LEAF_COLOUR).item(0).getTextContent());
				aveLenFlower.setMeasure(element.getElementsByTagName(ParserConstants.AVE_LEN_FLOWER).item(0).getAttributes().item(0).getTextContent());
				aveLenFlower.setValue(Integer.valueOf(element.getElementsByTagName(ParserConstants.AVE_LEN_FLOWER).item(0).getTextContent()));
				visualParameters.setAveLenFlower(aveLenFlower);
				flower.setVisualParameters(visualParameters);

				GrowingTips growingTips = new GrowingTips();
				Temperature temperature = new Temperature();
				Lighting lighting = new Lighting();
				Watering watering = new Watering();
				temperature.setMeasure(element.getElementsByTagName(ParserConstants.TEMPERATURE).item(0).getAttributes().item(0).getTextContent());
				temperature.setValue(Integer.valueOf(element.getElementsByTagName(ParserConstants.TEMPERATURE).item(0).getTextContent()));
				lighting.setLightRequiring(element.getElementsByTagName(ParserConstants.LIGHTING).item(0).getAttributes().item(0).getTextContent());
				watering.setMeasure(element.getElementsByTagName(ParserConstants.WATERING).item(0).getAttributes().item(0).getTextContent());
				watering.setValue(Integer.valueOf(element.getElementsByTagName(ParserConstants.WATERING).item(0).getTextContent()));
				growingTips.setTemperature(temperature);
				growingTips.setLighting(lighting);
				growingTips.setWatering(watering);

				flower.setGrowingTips(growingTips);
				flower.setMultiplying(element.getElementsByTagName(ParserConstants.MULTIPLYING).item(0).getTextContent());

				flowerList.add(flower);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		flowers.setFlowers(flowerList);

		return flowers;
	}

	public void save (String xmlFileName, Flowers flowers){
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();

			// root element
			Element root = document.createElement(ParserConstants.FLOWERS);
			root.setAttribute(XMLConstants.XMLNS_ATTRIBUTE, "http://www.nure.ua");
			document.appendChild(root);

			List<Flower> flowerList = flowers.getFlowers();

			for (int i = 0; i < flowerList.size(); i++) {
				// flower element
				Element flower = document.createElement(ParserConstants.FLOWER);
				root.appendChild(flower);

				// name element
				Element name = document.createElement(ParserConstants.NAME);
				name.appendChild(document.createTextNode(flowerList.get(i).getName()));
				flower.appendChild(name);

				// soil element
				Element soil = document.createElement(ParserConstants.SOIL);
				soil.appendChild(document.createTextNode(flowerList.get(i).getSoil()));
				flower.appendChild(soil);

				// origin element
				Element origin = document.createElement(ParserConstants.ORIGIN);
				origin.appendChild(document.createTextNode(flowerList.get(i).getOrigin()));
				flower.appendChild(origin);

				// visualParameters element
				Element visualParameters = document.createElement(ParserConstants.VISUAL_PARAMETERS);

				// stemColour element
				Element stemColour = document.createElement(ParserConstants.STEM_COLOUR);
				stemColour.appendChild(document.createTextNode(flowerList.get(i).getVisualParameters().getStemColour()));
				visualParameters.appendChild(stemColour);

				// stemColour element
				Element leafColour = document.createElement(ParserConstants.LEAF_COLOUR);
				leafColour.appendChild(document.createTextNode(flowerList.get(i).getVisualParameters().getLeafColour()));
				visualParameters.appendChild(leafColour);

				// aveLenFlower element
				Element aveLenFlower = document.createElement(ParserConstants.AVE_LEN_FLOWER);

				// set an attribute to aveLenFlower element
				aveLenFlower.setAttribute(ParserConstants.MEASURE, flowerList.get(i).getVisualParameters().getAveLenFlower().getMeasure());
				aveLenFlower.appendChild(document.createTextNode(flowerList.get(i).getVisualParameters().getAveLenFlower().getValue().toString()));
				visualParameters.appendChild(aveLenFlower);

				flower.appendChild(visualParameters);

				// growingTips element
				Element growingTips = document.createElement(ParserConstants.GROWING_TIPS);

				// temperature element
				Element temperature = document.createElement(ParserConstants.TEMPERATURE);
				// set an attribute to temperature element
				temperature.setAttribute(ParserConstants.MEASURE, flowerList.get(i).getGrowingTips().getTemperature().getMeasure());
				temperature.appendChild(document.createTextNode(flowerList.get(i).getGrowingTips().getTemperature().getValue().toString()));
				growingTips.appendChild(temperature);

				// lighting element
				Element lighting = document.createElement(ParserConstants.LIGHTING);
				// set an attribute to lighting element
				lighting.setAttribute(ParserConstants.LIGHT_REQUIRING, flowerList.get(i).getGrowingTips().getLighting().getLightRequiring());
				growingTips.appendChild(lighting);

				// watering element
				Element watering = document.createElement(ParserConstants.WATERING);
				// set an attribute to watering element
				watering.setAttribute(ParserConstants.MEASURE, flowerList.get(i).getGrowingTips().getWatering().getMeasure());
				watering.appendChild(document.createTextNode(flowerList.get(i).getGrowingTips().getWatering().getValue().toString()));
				growingTips.appendChild(watering);

				flower.appendChild(growingTips);

				// multiplying element
				Element multiplying= document.createElement(ParserConstants.MULTIPLYING);
				multiplying.appendChild(document.createTextNode(flowerList.get(i).getMultiplying()));
				flower.appendChild(multiplying);

			}

			// create the xml file
			// transform the DOM Object to an XML File
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(new File(xmlFileName));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(domSource, streamResult);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
