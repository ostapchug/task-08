package com.epam.rd.java.basic.task8.model;

public class Lighting {
    private String lightRequiring;

    public String getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }
}
