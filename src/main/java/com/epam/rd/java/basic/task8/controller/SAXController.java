package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private String currentTagName;
	private Flower flower;
	private VisualParameters visualParameters;
	private AveLenFlower aveLenFlower;
	private GrowingTips growingTips;
	private Temperature temperature;
	private Lighting lighting;
	private Watering watering;
	private List<Flower> flowerList;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		this.flowerList = new ArrayList<>();
	}

	public Flowers parse() {
		Flowers flowers = new Flowers();

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(new File(xmlFileName), this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		flowers.setFlowers(flowerList);

		return flowers;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentTagName = qName;

		switch (qName){
			case ParserConstants.FLOWER:
				flower = new Flower();
				break;
			case ParserConstants.VISUAL_PARAMETERS:
				visualParameters = new VisualParameters();
				flower.setVisualParameters(visualParameters);
				break;
			case ParserConstants.AVE_LEN_FLOWER:
				aveLenFlower = new AveLenFlower();
				aveLenFlower.setMeasure(attributes.getValue(ParserConstants.MEASURE));
				flower.getVisualParameters().setAveLenFlower(aveLenFlower);
				break;
			case ParserConstants.GROWING_TIPS:
				growingTips = new GrowingTips();
				flower.setGrowingTips(growingTips);
				break;
			case ParserConstants.TEMPERATURE:
				temperature = new Temperature();
				temperature.setMeasure(attributes.getValue(ParserConstants.MEASURE));
				growingTips.setTemperature(temperature);
				break;
			case ParserConstants.LIGHTING:
				lighting = new Lighting();
				lighting.setLightRequiring(attributes.getValue(ParserConstants.LIGHT_REQUIRING));
				growingTips.setLighting(lighting);
				break;
			case ParserConstants.WATERING:
				watering = new Watering();
				watering.setMeasure(attributes.getValue(ParserConstants.MEASURE));
				growingTips.setWatering(watering);
				break;
			default:
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		currentTagName = null;

		if (ParserConstants.FLOWER.equals(qName)){
			flowerList.add(flower);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String text = new String(ch, start, length);

		if (currentTagName != null){

			switch (currentTagName){
				case ParserConstants.NAME:
					flower.setName(text);
					break;
				case ParserConstants.SOIL:
					flower.setSoil(text);
					break;
				case ParserConstants.ORIGIN:
					flower.setOrigin(text);
					break;
				case ParserConstants.MULTIPLYING:
					flower.setMultiplying(text);
					break;
				case ParserConstants.STEM_COLOUR:
					visualParameters.setStemColour(text);
					break;
				case ParserConstants.LEAF_COLOUR:
					visualParameters.setLeafColour(text);
					break;
				case ParserConstants.AVE_LEN_FLOWER:
					aveLenFlower.setValue(Integer.valueOf(text));
					break;
				case ParserConstants.TEMPERATURE:
					temperature.setValue(Integer.valueOf(text));
					break;
				case ParserConstants.WATERING:
					watering.setValue(Integer.valueOf(text));
					break;
				default:
					break;
			}
		}
	}
}