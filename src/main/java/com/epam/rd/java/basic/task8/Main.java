package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.Flowers;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		boolean isValid = validate(xmlFileName);

		if (!isValid){
			return;
		}
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers flowersDOM = domController.parse();

		// sort (case 1)
		sort(flowersDOM.getFlowers(), Flower::getName);
		
		// save
		String outputXmlFile = "output.dom.xml";
		domController.save(outputXmlFile, flowersDOM);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		Flowers flowersSAX = saxController.parse();
		// sort  (case 2)
		sort(flowersDOM.getFlowers(), Flower::getOrigin);
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		domController.save(outputXmlFile, flowersSAX);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		Flowers flowersSTAX = staxController.parse();
		
		// sort  (case 3)
		sort(flowersDOM.getFlowers(), Flower::getMultiplying);
		
		// save
		outputXmlFile = "output.stax.xml";
		domController.save(outputXmlFile, flowersSTAX);
	}

	private static <T, U extends Comparable<? super U>> void sort(List<T> items, Function<T, U> extractor) {
		items.sort(Comparator.comparing(extractor));
	}

	private static boolean validate (String xmlFileName) {
		boolean result = false;
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Source schemaFile = new StreamSource(new File("input.xsd"));
		try {
			Schema schema = factory.newSchema(schemaFile);
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlFileName)));
			System.out.println(xmlFileName + " is valid");
			result = true;
		} catch (IOException | SAXException e) {
			System.out.println(xmlFileName + " is NOT valid, reason: " + e.getMessage());
		}

		return result;
	}

}
