package com.epam.rd.java.basic.task8.model;

public class AveLenFlower {
    private String measure;
    private Integer value;

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
